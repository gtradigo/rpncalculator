/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoox.rpn;

import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main class. Handles the I/O and interprets the commands from the user.
 *
 * @author gtradigo
 */
public class RPNCalculatorMain {

  private static final String VERSION = "0.1";

  private static final String PRINTSTACK_ON_COMMAND = "print stack on";
  private static final String PRINTSTACK_OFF_COMMAND = "print stack off";
  private static final String HELP_COMMAND = "help";
  private static final String QUIT_COMMAND = "quit";

  private static final String PROMPT = "> ";

  private static void out(String s) {
    System.out.println(PROMPT + s);
  }
  private static void err(String s) {
    System.err.println(PROMPT + s);
  }

  public static void main(String argv[]) {

    System.out.println("YOOX Assignment - Reverse Polish Notation Calculator, version " + VERSION);
    System.out.println("CRTL-c or \"quit\" to exit, \"print stack on(off)\" enables/disables the stack print, \"help\" for command list");
    System.out.println("Please enter each RPN expression on a line (CRTL-c or \"quit\" to exit):");

    RPNCalculator calc = new RPNCalculator();
    Scanner terminal = new Scanner(System.in);

    while (true) {
      String command = terminal.nextLine();
      if (command.equals(PRINTSTACK_ON_COMMAND)) {
        calc.enablePrintStack(true);
        out("stack print enabled");
      }
      else if (command.equals(PRINTSTACK_OFF_COMMAND)) {
        calc.enablePrintStack(false);
        out("stack print disabled");
      }
      else if (command.equals(HELP_COMMAND)) {
        out("Commands:");
        out("  print stack on   enables the print of the stack contents");
        out("  print stack off  disables the stack print");
        out("  help             prints this command list");
        out("  quit             exits the program");
      }
      else if (command.equals(QUIT_COMMAND)) {
        out("Bye!");
        System.exit(0);
      }
      else {
        double result;
        try {
          result = calc.evaluate(command);
          out(""+result);
        } catch (InvalidExpressionException ex) {
          err("Error with the expression: " + ex.getMessage());
        }
      }
    } // while
  }
}
