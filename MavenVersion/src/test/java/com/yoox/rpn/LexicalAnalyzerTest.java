package com.yoox.rpn;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for RPNCalculator.
 */
public class LexicalAnalyzerTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public LexicalAnalyzerTest( String testName ) {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite( LexicalAnalyzerTest.class );
    }

    /**
     * test 1 of evaluate method
     */
    public void testHasMoreTokens() {
      LexicalAnalyzer lex = new LexicalAnalyzer("3 2 +");
      assertTrue(lex.hasMoreTokens());
      assertTrue(lex.getNextToken().equals("3"));
      assertEquals(lex.getTokenType(), Tokens.NUM);
      assertEquals(3.0d, lex.getTokenVal());
      assertTrue(lex.hasMoreTokens());
      assertTrue(lex.getNextToken().equals("2"));
      assertTrue(lex.hasMoreTokens());
      assertTrue(lex.getNextToken().equals("+"));
      assertTrue(!lex.hasMoreTokens());
    }

}
