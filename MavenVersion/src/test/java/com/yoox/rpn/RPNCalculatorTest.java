package com.yoox.rpn;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for RPNCalculator.
 */
public class RPNCalculatorTest extends TestCase {

    private RPNCalculator calc;
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public RPNCalculatorTest( String testName ) {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite( RPNCalculatorTest.class );
    }

    public void init() {
      calc = new RPNCalculator();
    }

    /**
     * test 1 of evaluate method
     */
    public void testEvaluate1() {
      try {
        double result = calc.evaluate("4.0  3.0    -");
        assertEquals( 1.0d, result );
      }
      catch (Exception ex) { }
    }

    /**
     * test 2 of evaluate method
     */
    public void testEvaluate2() {
      try {
        double result = calc.evaluate("4.0 3.0 5.3 - *");
        assertEquals( 5.3d, result );
      }
      catch (Exception ex) { }
    }

    /**
     * test 2 of evaluate method
     */
    public void testEvaluate3() {
      boolean thrown = false;
      try {
        double result = calc.evaluate("4 3 7 + - /");
      }
      catch (Exception ex) {
        thrown = true;
      }
      assertTrue( thrown );
    }


}
