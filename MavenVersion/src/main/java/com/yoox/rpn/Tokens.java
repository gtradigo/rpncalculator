/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoox.rpn;

/**
 * Contains all of the valid RPN expressions tokens.
 *
 * @author gtradigo
 */
public interface Tokens {

  /** represents a "+" operation token */
  public int ADD = 0;
  /** represents a "-" operation token */
  public int SUB = 2;
  /** represents a "*" operation token */
  public int MUL = 3;
  /** represents a "/" operation token */
  public int DIV = 4;
  /** represents a "^" (power) operation token */
  public int POW = 5;
  /** represents a numeric operand token */
  public int NUM = 6;
  /** represents an unknown token */
  public int UNKNOWN = 7;
}
