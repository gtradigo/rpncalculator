/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoox.rpn;

/**
 * Exception thrown in case of a malformed RPN expression.
 *
 * @author gtradigo
 */
public class InvalidExpressionException extends Exception {

  public InvalidExpressionException() {
    super();
  }

  public InvalidExpressionException(String msg) {
    super(msg);
  }
}
