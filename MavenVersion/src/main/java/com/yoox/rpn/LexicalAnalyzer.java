/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoox.rpn;

/**
 * Simple lexical analyzer for the RPN Calculator.
 *
 * @author gtradigo
 */
public class LexicalAnalyzer implements Tokens {

  String[] tokens;
  int i;
  int tokenType;
  Double tokenVal;

  public LexicalAnalyzer(String expression) {
    tokens = expression.split("\\s+");
    i = 0;
    tokenType = UNKNOWN;
    tokenVal = null;
  }

  /**
   * This method should be called before getting the next token
   * to check if there are more available tokens in the expression.
   *
   * @return true if there are more tokens in the expression
   */
  public boolean hasMoreTokens() {
    return i < tokens.length;
  }

  /**
   * Returns the position of the current token
   *
   * @return the token position
   */
  public int getTokenNumber() {
    return i;
  }

  /**
   * If the current token is numeric, it returns its value, null otherwise
   *
   * @return a Double object containing the current numeric value of the token or null
   * @see #getTokenType
   */
  public Double getTokenVal() {
    return tokenVal;
  }

  /**
   * Returns the next token as a String.
   *
   * @return the next token
   */
  public String getNextToken() {
    String token = tokens[i++];
    tokenVal = null;
    try {
      tokenVal = Double.parseDouble(token);
    } catch (NumberFormatException ex) { }
    if (tokenVal != null) {
      tokenType = NUM;
    }
    else if (token.equals("+")) {
      tokenType = ADD;
    }
    else if (token.equals("-")) {
      tokenType = SUB;
    }
    else if (token.equals("*")) {
      tokenType = MUL;
    }
    else if (token.equals("/")) {
      tokenType = DIV;
    }
    else if (token.equals("^")) {
      tokenType = POW;
    }
    else {
      tokenType = UNKNOWN;
    }
    return token;
  }

  /**
   * Returns the token type.
   *
   * @return the token type
   * @see Tokens
   */
  public int getTokenType() {
    return tokenType;
  }
}
