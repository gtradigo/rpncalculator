/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yoox.rpn;

import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * A Reverse Polish Notation Calculator.
 *
 * @author gtradigo
 */
public class RPNCalculator implements Tokens {

  private LinkedList<Double> stack;
  private LexicalAnalyzer lex;
  private boolean printStack;

  /**
   * Default constructor
   */
  public RPNCalculator() {
    printStack = false;
  }

  /**
   * Enables/disables the print out of the stack contents
   *
   * @param b true to enable, false to disable
   */
  public void enablePrintStack(boolean b) {
    printStack = b;
  }

  /**
   * Evaluates an RPN expression string and returns the result
   *
   * @param expression the RPN expression string (e.g. "3 4 +")
   * @return the result of the expression as a double value
   * @throws InvalidExpressionException thrown in case of a malformed RPN expression
   */
  public double evaluate(String expression) throws InvalidExpressionException {

    try {
      String expr = expression.trim();
      if (expr.isEmpty()) {
        throw new Exception("Empty expression");
      }
      stack = new LinkedList<Double>();
      lex = new LexicalAnalyzer(expr);

      while (lex.hasMoreTokens()) {

        if (printStack) { System.out.println( stack ); }

        String token = lex.getNextToken();

        if (lex.getTokenType() == NUM) {
          stack.push( lex.getTokenVal() );
        }
        else if (lex.getTokenType() == ADD) {
          double op2 = stack.pop();
          double op1 = stack.pop();
          stack.push(op1 + op2);
        }
        else if (lex.getTokenType() == SUB) {
          double op2 = stack.pop();
          double op1 = stack.pop();
          stack.push(op1 - op2);
        }
        else if (lex.getTokenType() == MUL) {
          double op2 = stack.pop();
          double op1 = stack.pop();
          stack.push(op1 * op2);
        }
        else if (lex.getTokenType() == DIV) {
          double op2 = stack.pop();
          double op1 = stack.pop();
          stack.push(op1 / op2);
        }
        else if (lex.getTokenType() == POW) {
          double op2 = stack.pop();
          double op1 = stack.pop();
          stack.push(Math.pow(op1, op2));
        }
        else {
          throw new Exception("Token [" + token + "] at position " + lex.getTokenNumber() + " is invalid.");
        }
      }
      // when finished with a well formed expression, the result will be
      // on top of the stack, but it must be a single value
      if (stack.size() > 1) {
        throw new Exception("Please check the number of operands and operations");
      }
      return stack.pop();
    }
    catch (NoSuchElementException ex) {
      throw new InvalidExpressionException("Please check the number of operands and operations");
    }
    catch (Exception ex) {
      throw new InvalidExpressionException(ex.getMessage());
    }
  }

}
