# XOOY Assignment - Reverse Polish Notation Calculator

This project is a simple implementation of a Java Reverse Polish Notation (RPN) Calculator in Java.

## Getting Started

Testing should be simple. Just clone the project on your local machine and run Ant.

### Prerequisites

* JDK 6 or higher
* JUnit
* Ant or Maven

## Compiling and Running with Maven

The Maven version of the project is in the MavenVersion/ folder. To create the main jar, use the following maven command:

```
mvn clean package
```

which will also automatically run the JUnit tests. The project jar file will be generated in the target/ folder. To execute it, run:

```
java -jar target/RPNCalculator-0.1.jar
```

### Generating the APIs Documentation with Maven

The following command will generate the APIs documentation website:

```
mvn site
```

The entry HTML file to browse the documentation will be available in the target/site/index.html file.

## Compiling and Running with Ant

The Ant version is in the AntVersion/ folder. The default target is "main" which will perform a clean up, a build of the source code and a run. You can invoke the default target with:

```
ant
```

### Other Ant Targets

For information about other available targets type:

```
ant -p
```

### Generating the APIs Documentation

You can generate the documentation for the project APIs with:

```
ant docs
```

This will create a "docs" directory in which the "index.html" file is the entry point.

## Author

Giuseppe Tradigo
